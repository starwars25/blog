class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_comment, only: [:destroy, :update]
  before_action :correct_user, only: [:destroy]

  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash['success'] = 'Successfully created comment.'
      redirect_to post_path(@comment.post_id)
    else
      flash['danger'] = 'Invalid comment.'
      redirect_to post_path(@comment.post_id)
    end
  end

  def update
    if @comment.update_attributes(comment_params)
      render json: {result: 'success', id: @comment.id, author: @comment.user.nickname, content: @comment.content}
    else
      render json: {result: 'failure'}
    end
  end

  def destroy
    @comment.destroy
    flash['success'] = 'Deleted comment.'
    redirect_to post_path @comment.post.id
  end

  private
  def comment_params
    params.require(:comment).permit(:post_id, :content)
  end

  def find_comment
    @comment = Comment.find(params[:id])
  end

  def correct_user
    unless current_user.id == @comment.user_id
      flash['danger'] = 'You are not permitted to edit this comment.'
      redirect_to root_url
    end
  end


end
