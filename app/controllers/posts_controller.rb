class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_post, only: [:edit, :update, :destroy]

  def index
    @posts = Post.all.order('created_at desc')
    @post = Post.new
    @images = @post.images.build
  end

  def edit

  end

  def show
    @post = Post.includes(:images).find_by(id: params[:id])
    @comments = Comment.includes(:user).where("post_id = #{@post.id}")

  end

  def update

    if @post.user_id == current_user.id && @post.update_attributes(post_params)
      params[:images]['image'].each { |i| @image = @post.images.create(image: i) } if params[:images] && params[:images]['image']
      flash[:success] = 'Updated post.'
      redirect_to root_url
    else
      flash.now[:danger] = 'Failed to update post.'
      render 'posts/edit'
    end
  end

  def destroy
    if @post.user_id == current_user.id
      @post.destroy
      flash[:success] = 'Destroyed post.'
      redirect_to root_url
    else
      flash[:danger] = 'Failed to destroy post.'
      redirect_to root_url
    end
  end

  def create
    if current_user.owner
      post = Post.new(post_params)
      post.user_id = current_user.id
      if post.save
        params[:images]['image'].each { |i| @image = post.images.create(image: i) } if params[:images] && params[:images]['image']
        flash[:success] = 'Added new post.'
        redirect_to root_url
      else
        flash[:danger] = 'Failed to add post.'
        redirect_to root_url
      end
    else
      flash[:danger] = 'Failed to add post.'
      redirect_to root_url
    end

  end

  private
  def post_params
    params.require(:post).permit(:title, :description, images_attributes: [:id, :post_id, :image])
  end

  def find_post
    @post = Post.find_by(id: params[:id])
  end
end
