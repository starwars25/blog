class ImagesController < ApplicationController
  def destroy
    @image = Image.find(params[:id])
    @post = @image.post
    @image.destroy
    redirect_to edit_post_path @post
  end
end
