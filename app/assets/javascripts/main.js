var edit_form = function (event) {
    var id = event.target.id;
    var content = $('#content-' + id).text();
    $('#comment-' + id).html('<form><input type="text" id="form-content-' + id + '"><br><button id="' + id + '" onclick="submitButtonClicked(event)">Edit</button> </form>');
    $('#form-content-' + id).val(content);
};

var submitButtonClicked = function (event) {
    var id = event.target.id;
    var token = getCSRFToken();
    var adress = '/comments/' + id;
    var data = $('#form-content-' + id).val();
    var params = {
        authenticity_token: token,
        comment: {
            content: data
        }
    };
    var xhr = new XMLHttpRequest();
    xhr.open('PUT', adress, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send(JSON.stringify(params));
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var result = JSON.parse(xhr.responseText);
            if (result.result == 'failure') {
                alert('Enter other data.')
            } else if (result.result == 'success') {
                var table = document.createElement('table');
                var tr = document.createElement('tr');
                table.appendChild(tr);
                var author = document.createElement('td');
                author.innerHTML = result.author;
                tr.appendChild(author);
                var content = document.createElement('td');
                content.id = 'content-' + result.id;
                content.innerHTML = result.content;
                tr.appendChild(content);
                var delete_table = document.createElement('td');
                tr.appendChild(delete_table);
                var del = document.createElement('a');
                delete_table.appendChild(del);
                del.innerHTML = 'Delete';
                del.setAttribute('rel', 'nofollow');
                del.setAttribute('data-method', 'delete');
                del.setAttribute('href', '/comments/' + result.id);

                var edit_table = document.createElement('td');
                tr.appendChild(edit_table);
                var edit = document.createElement('a');
                edit_table.appendChild(edit);


                edit.innerHTML = 'Edit';
                edit.setAttribute('href', '#');
                edit.setAttribute('id', result.id);
                edit.setAttribute('onclick', 'edit_form(event)');

                $('#comment-' + id).html(table);


            }
        }
    };


};

var getCSRFToken = function () {
    var metas = document.getElementsByTagName('meta');
    for (var i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute('name') == 'csrf-token') {
            return metas[i].getAttribute('content');
        }
    }
};